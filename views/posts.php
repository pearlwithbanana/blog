<!DOCTYPE html>
<html lang="pl">
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <meta charset="utf-8"/>
</head>
<body>
<div class="container">
    <div class="navbar navbar-static-top bs-docs-nav">
        <div class="container">

            <nav id="bs-navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">

                    <li><a href="">Zaloguj</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="container">

        <table class="table table-bordered">
            <tr class="active">
                <td>UŻYTKOWNIK</td>
                <td>TYTUŁ</td>
                <td>DATA</td>
            </tr>
            <?php
            foreach ($posts as $postArray) :
                ?>
                <tr>
                    <td><?php echo $postArray['username']; ?></td>
                    <td><a href="SinglePost.php?id=<?php echo $postArray['id']; ?>"><?php echo $postArray['title']; ?></a>
                    </td>
                    <td><?php echo $postArray['date']; ?></td>
                </tr>
                <?php
            endforeach;
            ?>
        </table>
    </div>
</div>
</body>
</html>