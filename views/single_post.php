<!DOCTYPE html>
<html lang="pl">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <div class="container">
        <table class="table table-bordered">
            <?php foreach ($post as $postArray): ?>
            
                <tr class="active">
                    <td><h3><?php echo "Dodano przez: " . $postArray['username'] . ", " . $postArray['date']; ?></h3> </td>
                </tr>
                <tr>
                    <td><?php echo $postArray['content']; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
        <form method="post" action="AddComment.php">
            <div class="form-group">
                <label for="exampleInputNick1">Nick</label>
                <input type="text" class="form-control" id="exampleInputLogin1" name="commenterNick" placeholder="Anonymous">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Comment</label>
                <input type="text" class="form-control" id="exampleInputPassword1" name="commentContent" placeholder="Comment">
            </div>
            <button type="submit" class="btn btn-default">Skomentuj</button>
        </form>

    </div>