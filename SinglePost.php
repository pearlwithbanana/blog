<?php

require_once 'Models/BlogModel.php';
require_once 'Interfaces/IRender.php';

class SinglePost implements IRender {

    public function __construct() {
        if (isset($_GET['id'])) {
            $this->Render();
        } else {
            header("location: AllPosts.php");
        }
    }

    public function Render() {
        $post = $this->GetFromDatabase();
        if (isset($post)) {

            require_once 'views/single_post.php';
        } else {
            die('Database is empty or broken!');
        }
    }

    private function GetFromDatabase() {
        $id = $_GET['id'];
        $model = new BlogModel();
        $result = $model->GetSinglePost($id);
        if ($result->num_rows == 1) {
            return $result;
        } else {
            header("location: AllPosts.php");
        }
    }

}

new SinglePost();
