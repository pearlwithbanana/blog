<?php

require_once 'Models/BlogModel.php';
require_once 'Interfaces/IRender.php';

class AllPosts implements IRender {

    public function __construct() {
        $this->Render();
    }

    public function Render() {
        $posts = $this->GetFromDatabase();
        if (isset($posts)) {
            require_once 'views/posts.php';
        } else {
            die('Database is empty or broken!');
        }
    }

    private function GetFromDatabase() {
        $model = new BlogModel();
        $result = $model->GetAllPosts();
        return $result;
    }

}

new Allposts();
