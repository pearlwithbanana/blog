<?php

require_once 'Models/BlogModel.php';

class Authorize
{
    protected $validationErrors = array();
    protected $login;
    protected $password;

    public function __construct()
    {
        $this->Login();
    }

    private function FilterLoginForm()
    {
        $result = false;

        if (!(isset($_POST['username']))) {
            $this->validationErrors[] = "Podaj swój login";
        } else if (!(isset($_POST['password']))) {
            $this->validateErrors[] = "Podaj swoje hasło";
        } else {
            $this->login = $_POST['username'];
            $this->password = $_POST['password'];
            $result = true;
        }

        return $result;
    }

    private function CheckUserFromDb()
    {

        $model = new BlogModel();
        $authorization = $model->CheckUser();
        if(!$authorization)
        {
            $this->validationErrors[] = "Nieprawidłowa nazwa użytkownika lub hasło";
        }
    }

    private function CheckAuthorization($login, $password)
    {
        $this->SetUserSession($login);
        if ($this->DatabaseAuthorize($login, $password)) {
            header('Location: ' . 'admin.php');
        }
    }

    private function Login()
    {
        $this->FilterLoginForm();
        if($this->login && $this->password)
        {
            $this->CheckUserFromDb();
        }
        if(count($this->validationErrors) === 0 )
        {
            header('location: admin.php');
        }
    }

    private function SetUserSession($login)
    {
        $sessionData = array('user' => $login, 'isLoggedIn' => true);
        session_start($sessionData);
    }
}
