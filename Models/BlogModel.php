<?php

require_once 'Database.php';

class BlogModel extends Database
{

    public function __construct()
    {
        parent::__construct();
    }

    public function CheckUser($login, $password)
    {
        $this->OpenConnection();
        $sql = "SELECT password FROM user WHERE username=" . $login;

        $result = mysql_query($sql);
        $this->CloseConnection();
        if ($result->password == sha1($password)) {
            return true;
        }
        return false;
    }

    public function GetAllPosts()
    {
        $this->OpenConnection();
        $sql = "SELECT post.id, title, content, date, user.username as username FROM post LEFT JOIN user ON user.id = post.fk_user_id";

        $result = $this->db->query($sql);
        $this->CloseConnection();
        return $result;
    }

    public function GetSinglePost($id)
    {
        $this->OpenConnection();
        $sql = "SELECT title, content, date, user.username as username FROM post LEFT JOIN user ON user.id = post.fk_user_id WHERE fk_user_id=" . $id;

        $result = $this->db->query($sql);
        $this->CloseConnection();
        return $result;
    }

    public function CreatePost($userId, $title, $content, $date)
    {
        $this->OpenConnection();
        $sql = "INSERT INTO post VALUES ('', " . $userId . ", '" . $title . "', '" . $content . "', '" . $date . "')";

        $this->db->query($sql);
        $this->CloseConnection();
    }

    public function AddComment($comment, $date, $user = "Annonymous")
    {
        $this->OpenConnection();
        $sql = "INSERT INTO comment VALUES ('', '" . $user . "', '" . $comment . "', '" . $date . "')";

        $this->db->query($sql);
        $this->CloseConnection();
    }

    public function GetUserByLogin($login)
    {
        $this->OpenConnection();
        $sql = "SELECT id FROM user WHERE username = $login";
        $result = $this->db->query($sql);
        $this->CloseConnection();

        if ($result->id) {
            return true;
        }
        return false;
    }

}