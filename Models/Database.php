<?php

abstract class Database {

    protected $db;

    protected function __construct() {
        $this->OpenConnection();
    }

    protected function OpenConnection() {



        @$this->db = mysqli_connect("localhost", "root", "toor", "blog");
        if (!$this->db) {
            die("DATABASE CONNECTION ERROR, AUTHORIZATION FAILED OR INVALID CLIENT VERSION");
        }
    }

    protected function CloseConnection() {
        mysqli_close($this->db);
    }

}