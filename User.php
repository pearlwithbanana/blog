<?php

class User
{
    public $isLoggedIn; //bool

    public function __construct($location = false)
    {
        $this->isLoggedIn = $this->CheckUser();

        if($location)
        {
            $this->CheckUserAndRedirect($location);
        }
    }

    private function CheckUserAndRedirect($location)
    {
        if(isset($_SESSION['isLoggedIn']))
        {
            $this->isLoggedIn = true;
            header('Location: '."$location");
        }
        else {
            $this->isLoggedIn = false;
            header('Location: '.'Login.php');
        }
    }

    private function CheckUser()
    {
        if(isset($_SESSION['isLoggedIn'])) {
            return true;
        }
        return false;
    }
}

