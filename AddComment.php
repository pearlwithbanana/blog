<?php

require_once 'User.php';
require_once 'BlogModel.php';

class AddComment
{
    public function __construct()
    {
        $this->InsertNewComment();
    }

    private function InsertNewComment()
    {
        if (isset($_POST['commentContent'])) {
            $content = $_POST['commentContent'];
            $date = date('Y-m-d H:i:s');
            $model = new BlogModel();
            if (isset($_POST['commenterNick'])) {
                $commenter = $_POST['commenterNick'];
                $model->AddComment($content, $date, $commenter);
            } else {
                $model->AddComment($content, $date);
            }

        }
    }
}