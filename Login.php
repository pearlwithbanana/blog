<?php

require_once 'User.php';

class Login extends Authorize
{
    public function __construct()
    {
        parent::__construct();
        $this->LoadLoginView();
    }

    private function LoadLoginView()
    {
        $validationErrors = $this->validationErrors;
        require_once 'views/login.php';
    }

}

new Login();

